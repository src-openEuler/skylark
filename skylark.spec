Name:		skylark
Version:	1.0.0
Release:	7
Summary:	Skylark is a next-generation QoS-aware scheduler.

License:	Mulan PSL v2
URL:		https://gitee.com/openeuler/skylark
Source0:	%{name}-%{version}.tar.gz

Patch0001: guestinfo-Take-another-VM-stop-reason-to-account.patch
Patch0002: cpu_qos-Add-aditional-setting-for-cpu-QOS.patch
Patch0003: cachembw_qos-Add-a-job-to-sync-VM-pids-to-resctrl.patch
Patch0004: framework-create-pidfile-after-os.fork-in-child-proc.patch
Patch0005: cpu_qos-register-reset_domain_bandwidth-as-exit-func.patch
Patch0006: power_qos-cachembw_qos-Add-type-check-for-environmen.patch
Patch0007: docs-Use-chinese-description-instead-of-English-in-R.patch
Patch0008: Makefile-Use-strip-option-in-gcc.patch

BuildRequires:	python3-devel make gcc coreutils systemd-units
Requires:		python3 python3-APScheduler python3-libvirt
# For resource partition management
Requires:		systemd >= 249-32 libvirt >= 1.0.5
# For service management
Requires(post):	systemd-units
Requires(post):	systemd-sysv
Requires(preun): systemd-units
Requires(postun): systemd-units


%description
Skylark is a next-generation QoS-aware scheduler which provides coordinated resource scheduling for co-located applications with different QoS requirements.

%global debug_package %{nil}

%prep
%setup -q
%autopatch -p1


%build
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}


%files
%doc README.md README.en.md
%config /etc/sysconfig/*
/usr/sbin/*
/usr/lib/lib*
/usr/lib/systemd/system/*


%post
%systemd_post skylarkd.service
%preun
%systemd_preun skylarkd.service
%postun
%systemd_postun_with_restart skylarkd.service


%changelog
* Fri Dec 09 2022 Dongxu Sun <sundongxu3@huawei.com> - 1.0.0-7
- docs-Use-chinese-description-instead-of-English-in-R.patch
- Makefile-Use-strip-option-in-gcc.patch

* Sat Sep 03 2022 Dongxu Sun <sundongxu3@huawei.com> - 1.0.0-6
- cpu_qos: Register reset_domain_bandwidth as exit func after adding power_qos job
- power_qos/cachembw_qos: Add type check for environment variables

* Thu Aug 25 2022 Dongxu Sun <sundongxu3@huawei.com> - 1.0.0-5
- framework: create pidfile after os.fork in child process

* Fri Aug 19 2022 Keqian Zhu <zhukeqian1@huawei.com> - 1.0.0-4
- guestinfo: Take another VM stop reason to account
- cpu_qos: Add aditional setting for cpu QOS
- cachembw_qos: Add a job to sync VM pids to resctrl

* Wed Aug 10 2022 Keqian Zhu <zhukeqian1@huawei.com> - 1.0.0-3
- spec: Add missing dependencies of build and run

* Fri Aug 05 2022 Keqian Zhu <zhukeqian1@huawei.com> - 1.0.0-2
- Update baseline

* Thu Jul 28 2022 Keqian Zhu <zhukeqian1@huawei.com> - 1.0.0-1
- Package init
